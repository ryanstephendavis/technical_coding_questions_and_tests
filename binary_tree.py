#! /usr/bin/python3

from random import randint


class Node:
    def __init__(self, key):
        self.left = None
        self.right = None
        self.parent = None
        self.key = key


class BinaryTree:
    def __init__(self):
        self.root = None

    def insert(self, new_node):
        cur_parent = None
        cur_node = self.root
        while cur_node is not None:
            cur_parent = cur_node
            if new_node.key < cur_node.key:
                cur_node = cur_node.left
            else:
                cur_node = cur_node.right
        new_node.parent = cur_parent
        if cur_parent is None:  # Tree was empty
            self.root = new_node
        elif new_node.key < cur_parent.key:
            cur_parent.left = new_node
        else:  # new_node.key < cur_parent.key
            cur_parent.right = new_node

    def in_order_print(self, cur_node=None):
        if cur_node is None:
            cur_node = self.root
        if cur_node.left is not None:
            self.in_order_print(cur_node.left)
        print(cur_node.key)
        if cur_node.right is not None:
            self.in_order_print(cur_node.right)

    def post_order_print(self, cur_node=None):
        if cur_node is None:
            cur_node = self.root
        if cur_node.right is not None:
            self.post_order_print(cur_node.right)
        print(cur_node.key)
        if cur_node.left is not None:
            self.post_order_print(cur_node.left)

if __name__ == '__main__':
    BT = BinaryTree()
    node_list = [Node(randint(0, 100)) for _ in range(10)]
    for node in node_list:
        BT.insert(node)
    print('in order')
    BT.in_order_print()
    print('post order')
    BT.post_order_print()
