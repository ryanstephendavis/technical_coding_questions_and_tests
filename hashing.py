#! /usr/bin/python
"""
yes, this is silly because we have a pretty damn good hash implemented with Python's dict data structure, but this is
one of those exercises where we are attempting to implement a hash table with only arrays(python lists)

This implementation assumes that each Node's key is unique
Any collisions will append to a linked list
"""
from linked_list import Node


class HashTable:
    def __init__(self, size):
        self.big_list = list([] for _ in range(size))

    def pretty_print(self):
        for i, some_list in enumerate(self.big_list):
            print('index {}:'.format(i))
            for item in self.big_list[i]:
                print('\t{}'.format(item.key))

    def hash(self, key):
        return key % len(self.big_list)

    def insert(self, node):
        print('inserting node {} into index {}'.format(node.key, self.hash(node.key)))
        self.big_list[self.hash(node.key)].append(node)

    def get(self, key):
        ret_obj = None
        for potential_match in self.big_list[self.hash(key)]:
            if key == potential_match.key:
                ret_obj = potential_match
                break
        return ret_obj

    def remove(self, key):
        key_index = self.hash(key)
        for internal_list_index, potential_match in enumerate(self.big_list[key_index]):
            if key == potential_match.key:
                del self.big_list[key_index][internal_list_index]
                return True
        return False

if __name__ == '__main__':
    print('Running tests ...')
    ht = HashTable(10)

    node_x1 = Node(1, 'qwer')
    node_x2 = Node(11, 'heyooooo!!')
    node_x3 = Node(91, 'another node!!!')
    node_y = Node(2, 'asdf')
    node_z = Node(3, 'zxcv')

    ht.hash(node_x1.key == node_x2.key)
    ht.hash(node_x2.key == node_x3.key)
    ht.hash(node_x1.key == node_x3.key)

    # These should all hash to 1
    ht.insert(node_x1)
    ht.insert(node_x2)
    ht.insert(node_x3)
    # ...insert the rest
    ht.insert(node_y)
    ht.insert(node_z)
    ht.pretty_print()
    # Test expected indices have been appended to
    assert(len(ht.big_list[1]) == 3)
    assert(len(ht.big_list[2]) == 1)
    assert(len(ht.big_list[3]) == 1)
    assert(len(ht.big_list[4]) == 0)
    # Test get
    assert(ht.get(1) == node_x1)
    assert(ht.get(11) == node_x2)
    assert(ht.get(91) == node_x3)
    assert(ht.get(2) == node_y)
    assert(ht.get(3) == node_z)
    assert(ht.get(9) is None)
    assert(ht.get(10) is None)

    # Test the Delete
    assert(ht.remove(91))
    assert(len(ht.big_list[1]) == 2)
    assert(ht.remove(2))
    assert(len(ht.big_list[2]) == 0)
    assert(not ht.remove(9))
    print('SHOULD NOT HAVE 91 OR 2 left...')
    ht.pretty_print()

    print('All Passed')
