#!/usr/bin/python3
"""
This was an interview question ...

Write a function that returns the minimum number of moves a chess knight needs to move from a starting point to an end
point.  The coordinates 0, 0 exist and the board extends infinitely into the x and y directions.

EX board where starting coordinates are (2, 2) and the goal is (3, 4)

.
.
.
8
7
6
5
4      0
3
2    X
1
0
 0 1 2 3 4 5 . . .
"""
from collections import deque

def find_min_moves(start_x, start_y, end_x, end_y):
    num_moves = 0
    cur_node = Node(start_x, start_y, num_moves)
    visited_nodes = set([cur_node])
    queue_to_check = deque([cur_node])

    while len(queue_to_check) > 0:
        # Return number of moves if the current node is our endpoints
        cur_node = queue_to_check.popleft()
        if cur_node.x == end_x and cur_node.y == end_y: 
            return cur_node.num_moves

        # Expand adjacent nodes 
        node1 = Node(cur_node.x+2, cur_node.y+1, cur_node.num_moves+1)
        node2 = Node(cur_node.x+2, cur_node.y-1, cur_node.num_moves+1)
        node3 = Node(cur_node.x-2, cur_node.y+1, cur_node.num_moves+1)
        node4 = Node(cur_node.x-2, cur_node.y-1, cur_node.num_moves+1)
        node5 = Node(cur_node.x-1, cur_node.y+2, cur_node.num_moves+1)
        node6 = Node(cur_node.x+1, cur_node.y+2, cur_node.num_moves+1)
        node7 = Node(cur_node.x-1, cur_node.y-2, cur_node.num_moves+1)
        node8 = Node(cur_node.x+1, cur_node.y-2, cur_node.num_moves+1)
        expanded_nodes = [node1, node2, node3, node4, node5, node6, node7, node8]
        for node in expanded_nodes:
            if node_on_board(node) and node not in visited_nodes:
                visited_nodes.add(node)
                queue_to_check.append(node)  
    return -1 

def node_on_board(node):
    if node.x>=0 and node.y>=0:
        return True
    return False

class Node:
    def __init__(self, x, y, num_moves):
        self.x = x
        self.y = y
        self.num_moves = num_moves     

if __name__ == '__main__':
    assert(find_min_moves(2, 2, 3, 4) == 1)


'''
# my half-assed failed attempt
VISITED_NODES = Set()
Q = []

def find_min_moves(start_x, start_y, end_x, end_y, num_moves=0):
    VISITED_NODES.append((start_x, start_y))

    # If off the board ret
    if start_x < 0 or start_y < 0:
        ret - 1

    # If visited, ret
    if visited(start_x, start_y):
        ret - 1

    # if we’re at the goal, return
    if start_x == end_x and start_y == end_y:
        return num_moves


    # Add neighbors into Queue
    Q.push[(start_x + 2, start_y + 1)]
    Q.push[(start_x + 2, start_y - 1)]
    ...
    Q.push[(start_x + 1, start_y + 2)]

    # Search next neighbor
    n = Q.pop()
    if visited(n[0], n[1]):
        ret - 1
    else:
        return find_min_moves(n[0], n[1], end_x, end_y, num_moves += 1)

def visited(x, y):
    if (x, y) in VISITED_NODES:
        return True
    else:
        return False

if __name__ == '__main__':
    assert (find_min_moves(2, 2, 3, 4) == 1)
'''
