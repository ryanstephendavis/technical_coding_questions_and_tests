#!/usr/bin/python3

# INCOMPLETE!!

class Vertex:
    def __init__(self, unique_id, some_data):
        self.key = unique_id
        self.data = some_data


class AdjacencyListGraph:
    def __init__(self):
        self._vertices_to_connections = {}

    def pretty_print(self):
        for vertex, connections in self._vertices_to_connections.items():
            print('vertex "{}":\n\t{}'.format(vertex.unique_id, connections))

    def add_vertex(self, vertex):
        self._vertices_to_connections[vertex.unique_id] = []

    def remove_vertex(self, vertex):
        del self._vertices_to_connections[vertex.unique_id]


class AdjacencyMatrixGraph:
    def __init__(self, expected_num_of_vertices=5):
        self.key_to_index = {}
        self.adjacency_matrix = \
            list(list(None for _ in range(expected_num_of_vertices)) for _ in range(expected_num_of_vertices))

    def pretty_print(self):
        for row in self.adjacency_matrix:
            for entry in row:
                print('{}, '.format(entry), end='')
            print('')

if __name__ == '__main__':
    print('running sanity checks...')
    ListGraph = AdjacencyListGraph()
    ListGraph.pretty_print()

    MatrixGraph = AdjacencyMatrixGraph()
    MatrixGraph.pretty_print()
    print(len(MatrixGraph.adjacency_matrix))

