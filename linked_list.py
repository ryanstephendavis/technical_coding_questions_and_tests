#! /usr/bin/python


class Node:
    def __init__(self, key, info_str):
        self.key = key
        self.info = info_str
        self.next = None

    def pretty_print(self):
        print('Node {}, info="{}"'.format(self.key, self.info))


class SinglyLinkedList:
    def __init__(self):
        self.head_node = None

    def insert(self, node):
        if self.head_node is None:  # list is empty
            self.head_node = node
        else:  # walk through list and then append to tail
            cur_node = self.head_node
            while cur_node.next is not None:
                cur_node = cur_node.next
            cur_node.next = node

    def pretty_print(self):
        cur_node = self.head_node
        if cur_node is None:
            print('Empty List!')
        else:
            while cur_node is not None:
                cur_node.pretty_print()
                cur_node = cur_node.next

    def search(self, key):
        # Check head node first to avoid weird conditions
        if self.head_node.key == key:
            return self.head_node
        # Walk through until we find matching key
        cur_node = self.head_node.next
        while cur_node is not None:
            if cur_node.key == key:
                return cur_node
            cur_node = cur_node.next
        # Did not find a matching key
        return None

    def delete(self, key):
        """This could be made easier using a .prev in the Node class so we could simply search first"""
        """This returns the object if deleted, None if object not found"""
        # Check head node first to avoid weird conditions
        if self.head_node.key == key:
            self.head_node = self.head_node.next
            return self.head_node
        # Walk through until we find matching key
        cur_node = self.head_node.next
        prev_node = self.head_node
        while cur_node is not None:
            if cur_node.key == key:
                prev_node.next = cur_node.next
                return cur_node
            else:  # key not found, update loop invariants
                cur_node = cur_node.next
                prev_node = prev_node.next
        # Did not find a matching key
        return None

    def successor(self, key):
        node = self.search(key)
        return node.next

if __name__ == '__main__':
    print('Running tests ...')
    x = Node(1, 'qwer')
    y = Node(2, 'asdf')
    z = Node(3, 'zxcv')
    linked_list = SinglyLinkedList()

    # Test inserts
    linked_list.insert(x)
    linked_list.insert(y)
    linked_list.insert(z)
    linked_list.pretty_print()
    assert(linked_list.head_node == x)

    # Test search
    assert(linked_list.search(1) == x)
    assert(linked_list.search(2) == y)
    assert(linked_list.search(3) == z)
    assert(linked_list.search(4) is None)

    # Test delete
    linked_list.delete(2)
    print('ONLY 1 & 3 should be left')
    linked_list.pretty_print()
    assert(linked_list.search(2) is None)
    assert(linked_list.successor(1) == z)
    linked_list.delete(1)
    linked_list.delete(3)
    assert(linked_list.head_node is None)
    print('SHOULD BE EMPTY MEOW')
    linked_list.pretty_print()

    print('All Passed')
