#! /usr/bin/python

def is_palindrome(word):
    """
    This was asked during an interview to be coded on a whiteboard and is my 
    """
    mid_index = len(word)/2
    for i, left_side_letter in enumerate(word):
        if i > mid_index:
            break
        if left_side_letter != word[(i+1)*-1]:
            return False
    return True

def is_palindrome_verbosely_written(word):
    """
    This would be my intuition (to declare 'right_side_letter_index') for readability, but
    the firmware engineer seemed concerned about optimizing for memory usage 
    """
    mid_index = len(word)/2
    for i, letter in enumerate(word):
        if i > mid_index:
            break
        right_side_letter_index = (i+1)*-1
        right_side_letter = word[right_side_letter_index]
        if left_side_letter != right_side_letter:
            return False
    return True

if __name__ == '__main__':
    print('Running tests ...')
    assert(is_palindrome(''))
    assert(is_palindrome('a'))
    assert(is_palindrome('aa'))
    assert(is_palindrome('aba'))
    assert(is_palindrome('abba'))
    assert(is_palindrome('abcba'))
    assert(is_palindrome('deed'))
    assert(is_palindrome('racecar'))
    assert(not is_palindrome('ab'))
    assert(not is_palindrome('abc'))
    assert(not is_palindrome('dead'))
    assert(not is_palindrome('aabcaa'))
    print('All Passed')
